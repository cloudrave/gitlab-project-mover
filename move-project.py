import shutil

import os
import requests
import git
from git import Repo
import utils


# Defaults to create new repo with gitlab.com.
TO_API_BASE = os.getenv('TO_GITLAB_API_URL', "https://gitlab.com/api/v3/")

TO_TOKEN = os.getenv("TO_TOKEN", "")
FROM_TOKEN = os.getenv("FROM_TOKEN", "")
if not (TO_TOKEN and FROM_TOKEN):
    raise Exception("""Set FROM_TOKEN variable (representing your GitLab private token)
             and TO_TOKEN variable (representing your previous Gitlab private token).""")

FROM_API_BASE = os.getenv("FROM_GITLAB_API_URL")
if not FROM_API_BASE:
    raise Exception("Must set FROM_GITLAB_API_URL (e.g. https://111.11.11.111/api/v3/ )")

TO_HEADERS = {"PRIVATE-TOKEN": TO_TOKEN}
FROM_HEADERS = {"PRIVATE-TOKEN": FROM_TOKEN}

TO = 'TO'
FROM = 'FROM'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
TMP_DIR = os.path.join(BASE_DIR, 'tmp')

DEFAULT_SKIP_PATH = os.path.join(BASE_DIR, '.default_skip')


class GitProgressPrinter(git.RemoteProgress):
    def update(self, op_code, cur_count, max_count=None, message=''):
        if message:
            print "\t{}".format(message)


def get_gitlab_api_base_url(to_or_from):
    if to_or_from == TO:
        return TO_API_BASE
    elif to_or_from == FROM:
        return FROM_API_BASE
    else:
        raise Exception("Must be 'TO' or 'FROM'")


def get_gitlab_headers(to_or_from):
    if to_or_from == TO:
        return TO_HEADERS
    elif to_or_from == FROM:
        return FROM_HEADERS
    else:
        raise Exception("Must be 'TO' or 'FROM'")


class RecoverableException(Exception):
    pass


def get_namespace_id(search_string, to_or_from):
    response = requests.get(
        get_gitlab_api_base_url(to_or_from) + "namespaces",
        data=dict(search=search_string),
        headers=get_gitlab_headers(to_or_from)
    )
    namespaces = utils.check_response(response, 'getting namespace id for {}'.format(search_string))
    if len(namespaces) == 0:
        raise RecoverableException("Error: Namespace '{search_string}' does not exist at {url}.".format(
            search_string=search_string,
            url=get_gitlab_api_base_url(to_or_from)
        ))
    return namespaces[0]["id"]


def create_project(path, namespace_path, to_or_from, visibility_level=0):
    query = dict(
        name=path,
        path=path,
        visibility_level=visibility_level,
    )

    if namespace_path:
        namespace_id = get_namespace_id(namespace_path, to_or_from)
        query['namespace_id'] = namespace_id

    response = requests.post(
        get_gitlab_api_base_url(to_or_from) + "projects",
        data=query,
        headers=get_gitlab_headers(to_or_from),
    )
    return utils.check_response(response, 'creating project')


def get_project_json(path, namespace_path, to_or_from):
    response = requests.get(get_gitlab_api_base_url(to_or_from) + 'projects/{namespace_path}%2F{path}'.format(
        namespace_path=namespace_path,
        path=path,
    ), headers=get_gitlab_headers(to_or_from))
    return utils.check_response(response, 'getting project details at {}'.format(get_gitlab_api_base_url(to_or_from)))


def clone_entire_repo(existing_ssh_url, download_path):
    path = os.path.join(TMP_DIR, download_path)
    shutil.rmtree(TMP_DIR)  # Remove temporary directory.
    os.mkdir(TMP_DIR)
    return Repo.clone_from(existing_ssh_url, path, progress=GitProgressPrinter(), mirror=True)


class ProjectInfo:
    descriptor = None
    namespace_path = None
    path = None

    def __init__(self, descriptor='Project Information', namespace_path=None, path=None, default_path=''):
        self.descriptor = descriptor
        if namespace_path is None or path is None:
            self.load_from_user(default_path=default_path)
        else:
            self.namespace_path = namespace_path
            self.path = path

    def load_from_user(self, default_path=''):
        print(self.descriptor)
        print(reduce(lambda x, l: x + '-', self.descriptor, ''))
        self.namespace_path = raw_input("Namespace (leave empty if user namespace): ")
        self.path = raw_input("Path (e.g. 'my-cool-project') [{}]: ".format(default_path))
        if not self.path:
            self.path = default_path


def move_project(from_details=None, to_details=None, default_to_path=''):
    """
    :type from_details: ProjectInfo
    :type to_details: ProjectInfo
    :type default_to_path: str
    """
    print

    if from_details is None:
        from_details = ProjectInfo('Existing Project Information')
    if to_details is None:
        to_details = ProjectInfo('New Project Information', default_path=default_to_path)

    # Clone existing repo locally.
    print("Cloning existing repository locally...")
    from_json = get_project_json(from_details.path, from_details.namespace_path, FROM)
    repo = clone_entire_repo(from_json['ssh_url_to_repo'], download_path=from_json['path'])

    # Create new GitLab project.
    print("Creating new project on GitLab...")
    to_json = create_project(to_details.path, to_details.namespace_path, TO)

    # Add new GitLab remote, and push local repo to it.
    print("Pushing existing repository to new repository...")
    repo.create_remote('to_gitlab', to_json['ssh_url_to_repo'])
    repo_new_remote = repo.remote('to_gitlab')
    repo_new_remote.push(progress=GitProgressPrinter(), mirror=True)

    add_default_skip_project(from_json)
    print("Done. Access your new GitLab project at {}".format(to_json['web_url']))


def list_projects(to_or_from):
    return utils.list_projects(get_gitlab_api_base_url(to_or_from), get_gitlab_headers(to_or_from))


def should_skip_by_default(project_json):
    if not os.path.exists(DEFAULT_SKIP_PATH):
        return False
    with open(DEFAULT_SKIP_PATH, 'r') as skip_file:
        for line in skip_file:
            if line.replace('\n', '') == project_json['path_with_namespace']:
                return True
    return False


def add_default_skip_project(project_json):
    with open(DEFAULT_SKIP_PATH, 'a') as skip_file:
        skip_file.write(project_json['path_with_namespace'] + '\n')


def move_all_projects():
    from_projects = list_projects(FROM)
    for from_project in from_projects:
        if should_skip_by_default(from_project):
            print("Skipping {}".format(from_project['path_with_namespace']))
            continue

        print('')
        print("BEGINNING MOVE FOR {}".format(from_project['path_with_namespace']))
        print('')

        skip = raw_input("Skip this project [y/N]? ").upper()
        if skip == 'Y':
            add_default_skip_project(from_project)
            continue

        while True:
            try:
                project_info = ProjectInfo(
                    namespace_path=from_project['namespace']['path'],
                    path=from_project['path'],
                )
                move_project(from_details=project_info, default_to_path=from_project['path'])
                break
            except (RecoverableException, utils.CheckResponseException) as exc:
                print(exc)
                continue

    print("Considered {} projects for moving.".format(len(from_projects)))


def __main__():
    print("Single project or All projects?")
    print("^                 ^            ")
    mode = raw_input('S/A: ').upper()
    if mode == 'S':
        move_project()
    elif mode == 'A':
        move_all_projects()

if __name__ == '__main__':
    __main__()
