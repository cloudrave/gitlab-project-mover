import os
import requests
import utils


# Defaults to create new repo with gitlab.com.
API_BASE = os.getenv('API_URL', "https://gitlab.com/api/v3/")

API_TOKEN = os.getenv("API_TOKEN", "")
if not API_TOKEN:
    raise Exception("""Set API_TOKEN variable (representing your GitLab private token)""")

HEADERS = {"PRIVATE-TOKEN": API_TOKEN}


def list_projects():
    return utils.list_projects(API_BASE, HEADERS)


def delete_project(project):
    response = requests.delete(API_BASE + 'projects/{}'.format(project['id']), headers=HEADERS)
    return utils.check_response(response, 'deleting {}'.format(project['path_with_namespace']))


def __main__():
    projects = list_projects()
    yn = raw_input("This will delete ALL projects. Continue? [y/N]: ")
    if yn.upper() != 'Y':
        return
    for project in projects:
        print("Deleting {}...".format(project['path_with_namespace']))
        delete_project(project)

__main__()
