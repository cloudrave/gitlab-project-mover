import json

import sys

import time

import requests
from tqdm import tqdm


class CheckResponseException(Exception):
    pass


def check_response(response, failed_goal):
    if not response.ok:
        raise CheckResponseException("Error {}: \n{}".format(failed_goal, json.dumps(response.json(), indent=4)))
    return response.json()


def list_projects(api_url_base, headers):
    print("Loading projects... ")
    tqdm()

    def list_project_page(_page_num):
        """
        :rtype: (list[dict[str, T]], int)
        """
        _projects = []
        response = requests.get(api_url_base + 'projects?page={page_num}'.format(
            page_num=_page_num
        ), headers=headers)
        _projects += check_response(response, 'loading projects')
        return _projects, int(response.headers['X-Total-Pages'])

    (projects, total_pages) = list_project_page(1)
    print("Loading multiple project pages...")
    if total_pages > 1:
        progress = tqdm(xrange(2, total_pages + 1), unit='page', total=total_pages, initial=1, maxinterval=1)
        for page_num in progress:
            projects += list_project_page(page_num)[0]
            time.sleep(2)
    print('')
    return projects
